package com.example.oleksii_borys.recyclerviewsample;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.Toast;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements SampleAdapter.OnItemClickListener {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler);

        SampleAdapter adapter = new SampleAdapter(Arrays.asList(getResources().getStringArray(android.R.array.phoneTypes)), this);

        ItemTouchHelper ith =new  ItemTouchHelper( new SwipeToDeleteCallback(adapter));
        ith.attachToRecyclerView(recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClicked(String item) {
        Toast.makeText(this, item + " clicked", Toast.LENGTH_SHORT).show();
    }
}
