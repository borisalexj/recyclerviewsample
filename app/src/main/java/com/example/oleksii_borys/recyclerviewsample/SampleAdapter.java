package com.example.oleksii_borys.recyclerviewsample;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleksii Borys on 3/2/2018.
 */

class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.ViewHolder> implements SwipeAdapter {

    private OnItemClickListener clickListener;
    private int selected = -1;
    private ArrayList<String> strings = new ArrayList<>();

    public SampleAdapter(List<String> strings, OnItemClickListener listener) {
        this.strings.clear();
        this.strings.addAll(strings);
        this.clickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemTextView.setText(strings.get(position));
        if (position == selected){
            holder.itemTextView.setBackgroundColor(ContextCompat.getColor(holder.itemTextView.getContext(), R.color.colorPrimary));
        } else {
            holder.itemTextView.setBackgroundColor(ContextCompat.getColor(holder.itemTextView.getContext(), android.R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    @Override
    public void onSwipe(int position) {
        strings.remove(position);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView itemTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            itemTextView = itemView.findViewById(R.id.itemTextView);
            itemTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClicked(strings.get(getAdapterPosition()));
            }
        }
    }

    interface OnItemClickListener {
        void onItemClicked(String item);
    }
}
