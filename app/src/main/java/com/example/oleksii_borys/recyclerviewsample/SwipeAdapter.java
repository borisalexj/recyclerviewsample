package com.example.oleksii_borys.recyclerviewsample;

interface SwipeAdapter {
    public void onSwipe(int position);
}
